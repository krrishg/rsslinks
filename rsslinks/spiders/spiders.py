import scrapy

class YoutubeSpider(scrapy.Spider):
    name = "youtube"

    def __init__(self, url=None, *args, **kwargs):
        super(YoutubeSpider, self).__init__(*args, **kwargs)
        self.start_urls = [url]

    def parse(self, response):
        prefix = "https://www.youtube.com/feeds/videos.xml?channel_id="
        xpath_query = "//meta[@itemprop='channelId']/@content"
        print(prefix + response.xpath(xpath_query).get())
